-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.45 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных weballergy
CREATE DATABASE IF NOT EXISTS `weballergy` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `weballergy`;

-- Дамп структуры для таблица weballergy.allergen
CREATE TABLE IF NOT EXISTS `allergen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.diagnosis
CREATE TABLE IF NOT EXISTS `diagnosis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.diary
CREATE TABLE IF NOT EXISTS `diary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DateDiary` date DEFAULT NULL,
  `Note` varchar(1000) DEFAULT NULL,
  `idUser` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_diary_user` (`idUser`),
  CONSTRAINT `FK_diary_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.diary_medicine
CREATE TABLE IF NOT EXISTS `diary_medicine` (
  `idMedicine` int(11) DEFAULT NULL,
  `idDiary` int(11) DEFAULT NULL,
  KEY `FK__medicine` (`idMedicine`),
  KEY `FK__diary` (`idDiary`),
  CONSTRAINT `FK__diary` FOREIGN KEY (`idDiary`) REFERENCES `diary` (`id`),
  CONSTRAINT `FK__medicine` FOREIGN KEY (`idMedicine`) REFERENCES `medicine` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.diary_symptom
CREATE TABLE IF NOT EXISTS `diary_symptom` (
  `idSymptom` int(11) DEFAULT NULL,
  `idDiary` int(11) DEFAULT NULL,
  `Deg` int(11) DEFAULT NULL,
  KEY `FK_Diary_Symptom_symptom` (`idSymptom`),
  KEY `FK_Diary_Symptom_diary` (`idDiary`),
  CONSTRAINT `FK_Diary_Symptom_diary` FOREIGN KEY (`idDiary`) REFERENCES `diary` (`id`),
  CONSTRAINT `FK_Diary_Symptom_symptom` FOREIGN KEY (`idSymptom`) REFERENCES `symptom` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.doctor_patient
CREATE TABLE IF NOT EXISTS `doctor_patient` (
  `idPatient` int(11) DEFAULT NULL,
  `idDoctor` int(11) DEFAULT NULL,
  KEY `FK__patient` (`idPatient`),
  KEY `FK__patient_2` (`idDoctor`),
  CONSTRAINT `FK__patient` FOREIGN KEY (`idPatient`) REFERENCES `patient` (`id`),
  CONSTRAINT `FK__patient_2` FOREIGN KEY (`idDoctor`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.extrainf
CREATE TABLE IF NOT EXISTS `extrainf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idDiary` int(11) DEFAULT NULL,
  `Diagnosys` int(11) DEFAULT NULL,
  `LifePlace` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_extrainf_diary` (`idDiary`),
  CONSTRAINT `FK_extrainf_diary` FOREIGN KEY (`idDiary`) REFERENCES `diary` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.extrainf_allergen
CREATE TABLE IF NOT EXISTS `extrainf_allergen` (
  `idExtraInf` int(11) DEFAULT NULL,
  `idAllergen` int(11) DEFAULT NULL,
  KEY `FK__extrainf` (`idExtraInf`),
  KEY `FK__allergen` (`idAllergen`),
  CONSTRAINT `FK__allergen` FOREIGN KEY (`idAllergen`) REFERENCES `allergen` (`id`),
  CONSTRAINT `FK__extrainf` FOREIGN KEY (`idExtraInf`) REFERENCES `extrainf` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.medicine
CREATE TABLE IF NOT EXISTS `medicine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.patient
CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Surname` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Patronymic` varchar(100) DEFAULT NULL,
  `Sex` varchar(3) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `idDiagnosis` int(11) DEFAULT NULL,
  `LifePlace` varchar(100) DEFAULT NULL,
  `idUser` int(10) unsigned DEFAULT NULL,
  `isDoctor` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__user` (`idUser`),
  KEY `FK_patient_diagnosis` (`idDiagnosis`),
  CONSTRAINT `FK_patient_diagnosis` FOREIGN KEY (`idDiagnosis`) REFERENCES `diagnosis` (`id`),
  CONSTRAINT `FK__user` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.symptom
CREATE TABLE IF NOT EXISTS `symptom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица weballergy.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Avatar` varchar(250) DEFAULT NULL,
  `Salt` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
